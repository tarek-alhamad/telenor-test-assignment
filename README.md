# Telenor-Test-Assignment

Use the provided template to make a single page web application displaying a responsive web interface with a header, footer and content.
The content will be fetched from the API, displaying a list of appointments that could be a call or chat with a customer service agent.
Your responsibility is to update the existing api and create the frontend part of this application so that you can achieve the requirements bellow.

- - - -

## Provided template contains:

-	Asp.NET Core Application, configured with all the basic functionalities (saving data, exception handling, Swagger UI, extensions, class helpers)
-	The ORM used is Entity Framework Core
-	Web API configured with two methods (GET, POST)
-	Sample Json file containing reservations (Resources\Reservations.json)


- - - -
## Requirements
- ###  Backend: Each new reservation created will be saved by default in the same day of creation. So taking that into consideration your responsibility is:
1. When adding a new reservation/s is to check if there is any conflict with an existing reservation and return the proper error message such as SamePeriod, OverlapsWith and IntersectsWith.
2. Write an API method for saving the reservations from the provided Json file.
3. It would be valuable if you can provide unit/integration tests for the new api method mentioned in the previous point.

- ### Frontend: You can use any JavaScript framework (Angular, React, Vue, etc)
1. A single web page application (separated/integrated with the backend application).
2. In the web page the user can see all the reservations he has in the same day.
3. The user can upload a list of reservations to the newly created api from the json file.
4. The user should be able to add new, update or delete existing reservation.

- - - -

## Evaluation

-	Finishing the assignment is valuable, but the main point for us is to evaluate structure and the quality of the code.
-	You can upload your code to  GitHub/Bitbucket repo or send to us in a zip file.
-	Time consumed to finish the task is highly evaluated as well.

## Project setup on your local device:
1. Clone this repo to your local device. 
2. Restore NuGet packages and build the application. 
3. Make sure to make any update to the appsettings.json (server) to match your local sql instance connection string. 
4. In visual studio you can navigate to Quick launch and open Package Manager Console and run the command "update-database".
5. Run the application to make sure you get to Swagger UI interface. 

- - - -

Best of luck!

### Telenor.se Team
