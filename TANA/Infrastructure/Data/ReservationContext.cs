﻿using Core.Entities.Reservations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Data
{
    public class ReservationContext : DbContext
    {
        public ReservationContext(DbContextOptions<ReservationContext> options)
            : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Reservation>(ConfigureReservation);
        }
        private void ConfigureReservation(EntityTypeBuilder<Reservation> builder)
        {
            builder.ToTable("Reservations");
            builder.HasKey(re => re.Id);
            builder.OwnsOne(re => re.ContactInfo);
            builder.Property(re => re.Start).
                IsRequired(true);
            builder.Property(re => re.End).
                IsRequired(true);
            builder.HasIndex(re => new { re.Start, re.End, re.Date }).IsUnique(true);
        }
    }
}