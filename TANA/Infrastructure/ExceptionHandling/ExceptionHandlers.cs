﻿using Core.Constants;
using Core.Exceptions;
using Infrastructure.Responses;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace Infrastructure.ExceptionHandling
{
    public class DbUpdateExceptionHandler : BaseExceptionHandler
    {
        public override bool CanHandleException(Exception ex)
        {
            if (!(ex is DbUpdateException))
            {
                return false;
            }

            Exception = ex;
            return true;
        }

        public override Task FormatResponse(HttpResponse response)
        {
            response.StatusCode = 409;
            ApiResponse apiResponse = new ApiResponse(ErrorMessages.DbUpdateException, Exception.InnerException?.Message);
            return response.WriteAsync(JsonConvert.SerializeObject(apiResponse));
        }
    }

    public class ArgumentExceptionHandler : BaseExceptionHandler
    {
        public override bool CanHandleException(Exception ex)
        {
            if (!(ex is ArgumentException))
            {
                return false;
            }

            Exception = ex;
            return true;
        }
        public override Task FormatResponse(HttpResponse response)
        {
            response.StatusCode = 406;
            ApiResponse apiResponse = new ApiResponse(ErrorMessages.ArgumentException, Exception.Message);
            return response.WriteAsync(JsonConvert.SerializeObject(apiResponse));
        }
    }

    public class ArgumentNullExceptionHandler : BaseExceptionHandler
    {
        public override bool CanHandleException(Exception ex)
        {
            if (!(ex is ArgumentNullException))
            {
                return false;
            }

            Exception = ex;
            return true;
        }
        public override Task FormatResponse(HttpResponse response)
        {
            response.StatusCode = 400;
            ApiResponse apiResponse = new ApiResponse(ErrorMessages.ArgumentException, Exception.Message);
            return response.WriteAsync(JsonConvert.SerializeObject(apiResponse));
        }
    }

    public class ExceptionHandler : BaseExceptionHandler
    {
        public override bool CanHandleException(Exception ex)
        {

            if (!(ex is Exception))
            {
                return false;
            }

            Exception = ex;
            return true;
        }

        public override Task FormatResponse(HttpResponse response)
        {
            response.StatusCode = 500;
            ApiResponse apiResponse = new ApiResponse(ErrorMessages.Exception, Exception.Message);
            return response.WriteAsync(JsonConvert.SerializeObject(apiResponse));
        }
    }
}
