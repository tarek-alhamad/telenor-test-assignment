﻿using Core.Exceptions;
using Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Infrastructure.ExceptionHandling
{
    public static class ExceptionHandlerFactory
    {
        public static IExceptionHandler GetErrorHandler(Exception ex)
        {
            Assembly assembly = AppDomain.CurrentDomain.GetAssemblies().SingleOrDefault(am => am.GetName().Name == "Infrastructure");
            IEnumerable<Type> allErrorHandlers = assembly.GetTypes().Where(type => typeof(BaseExceptionHandler).IsAssignableFrom(type) && type != typeof(BaseExceptionHandler));
            IExceptionHandler errorHandler = allErrorHandlers.Select(eh => (IExceptionHandler)Activator.CreateInstance(eh))
                .FirstOrDefault(ha => ha.CanHandleException(ex));
            return errorHandler;

        }
    }
}
