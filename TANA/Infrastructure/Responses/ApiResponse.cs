﻿using Newtonsoft.Json;

namespace Infrastructure.Responses
{
    public class ApiResponse
    {
        public ApiResponse(string error, string message)
        {
            Error = error;
            Message = message;
        }
        public ApiResponse(bool isSuccess, string message, object data = null)
        {
            IsSuccess = isSuccess;
            Message = message;
            Data = data;
        }

        [JsonProperty("data")]
        public object Data { get; set; }

        [JsonProperty("error")]
        public string Error { get; set; }

        [JsonProperty("isSuccess")]
        public bool IsSuccess { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }
    }
}
