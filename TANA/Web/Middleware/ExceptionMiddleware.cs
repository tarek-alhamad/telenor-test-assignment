﻿using Infrastructure.ExceptionHandling;
using Microsoft.AspNetCore.Http;
using System;
using System.Threading.Tasks;

namespace Web.Middleware
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;

        public ExceptionMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (Exception ex)
            {
                await FormatResponse(httpContext, ex);
            }
        }

        private static Task FormatResponse(HttpContext context, Exception exception)
        {
            context.Response.ContentType = "application/json";
            return ExceptionHandlerFactory.GetErrorHandler(exception).FormatResponse(context.Response);

        }
    }
}