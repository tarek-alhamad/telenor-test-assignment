﻿using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;

namespace Web.Filters
{
    public class ExceptionHandlingFilter : ExceptionFilterAttribute
    {
        private readonly ILogger<ExceptionHandlingFilter> _logger;

        public ExceptionHandlingFilter(ILogger<ExceptionHandlingFilter> logger)
        {
            _logger = logger;
        }

        public override void OnException(ExceptionContext context)
        {
            LogError(context);
        }

        private void LogError(ExceptionContext context)
        {
            var exception = context.Exception;
            var actionInformation = context.ActionDescriptor;
            _logger.LogError($"{actionInformation.DisplayName} error", exception);
            context.ExceptionHandled = false;

        }
    }
}
