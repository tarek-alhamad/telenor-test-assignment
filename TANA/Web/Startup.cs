﻿using Core.Interfaces;
using Infrastructure.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.IO;
using System.Reflection;
using Web.Filters;
using Web.Helpers;
using Web.Interfaces;
using Web.Middleware;

namespace Web
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
            services.AddDbContext<ReservationContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("ReservationContext")));

            ConfigureDependencies(services);
            services.AddMvc(options =>
            {
                options.Filters.Add(new ServiceFilterAttribute(typeof(ExceptionHandlingFilter)));

            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            ConfigureSwagger(services);
        }

        private void ConfigureDependencies(IServiceCollection services)
        {
            services.AddSingleton<ExceptionHandlingFilter>();
            services.AddSingleton<ExceptionMiddleware>();
            services.AddScoped(typeof(IRepository<>), typeof(EfRepository<>));
            services.AddScoped<IReservationViewModelHelper, ReservationViewModelHelper>();
        }

        private void ConfigureSwagger(IServiceCollection services)
        {
            services.AddSwaggerGen(swagGen =>
            {
                swagGen.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "Reservations System API",
                    Description = "An application to help service agent manage and avoid conflicts between reservations.",
                    TermsOfService = "None",
                    Contact = new Contact
                    {
                        Name = "Telenor Group",
                        Url = "https://www.telenor.se"
                    }
                });
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                swagGen.IncludeXmlComments(xmlPath);
            });

        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseMiddleware<ExceptionMiddleware>();
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Reservations System API V1");
                c.RoutePrefix = string.Empty;
            });

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}