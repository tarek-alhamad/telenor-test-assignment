﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Web.Models.ReservationViewModels;

namespace Web.Interfaces
{
    public interface IReservationViewModelHelper
    {
        Task<ReservationViewModel> AddReservation(CreateReservationViewModel model);
        Task<IEnumerable<ReservationViewModel>> ListTodayReservations();
    }
}