﻿using Infrastructure.Responses;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Web.Extensions;
using Web.Interfaces;
using Web.Models.ReservationViewModels;

namespace Web.Controllers.Api
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class ReservationsController : ControllerBase
    {
        private readonly IReservationViewModelHelper _reservationViewModelHelper;
        private const string ListOfTodayReservation = "List of today reservations";
        private const string ReservationAdded = "Reservation added successfully";
        private const string ModelValidationError = "Model Validation Error";

        public ReservationsController(IReservationViewModelHelper reservationViewModelHelper)
        {
            _reservationViewModelHelper = reservationViewModelHelper;
        }

        /// <summary>
        /// List today reservations
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ActionName("ListAll")]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        public async Task<IActionResult> ListAll()
        {
            var reservationViewModels = await _reservationViewModelHelper.ListTodayReservations();

            return Ok(new ApiResponse(true, ListOfTodayReservation, reservationViewModels));
        }


        /// <summary>
        /// Add New Reservation -timeFormat: HH:mm - ex 18:30
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("AddReservation")]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        public async Task<IActionResult> AddReservation([FromForm]CreateReservationViewModel model)
        {
            var modelValidation = model.Validate().ToList();
            if (modelValidation.Any())
            {
                return BadRequest(new ApiResponse(ModelValidationError, modelValidation.ConvertToString()));
            }
            var reservationViewModel = await _reservationViewModelHelper.AddReservation(model);

            return Ok(new ApiResponse(true, ReservationAdded, reservationViewModel));
        }
    }
}