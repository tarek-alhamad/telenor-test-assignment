﻿using Core.Entities.Reservations;
using Core.Interfaces;
using Core.Specifications.ReservationSpecifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Extensions;
using Web.Interfaces;
using Web.Models.ReservationViewModels;

namespace Web.Helpers
{
    public class ReservationViewModelHelper : IReservationViewModelHelper
    {

        private readonly IRepository<Reservation> _reservationRepository;

        public ReservationViewModelHelper(IRepository<Reservation> reservationRepository)
        {
            _reservationRepository = reservationRepository;
        }

        public async Task<ReservationViewModel> AddReservation(CreateReservationViewModel model)
        {
            var reservation = model.ToReservation();
            await _reservationRepository.AddAsync(reservation);
            var reservationViewModel = new ReservationViewModel(reservation);

            return reservationViewModel;
        }

        public async Task<IEnumerable<ReservationViewModel>> ListTodayReservations()
        {
            var spec = new ReservationSpecification(DateTime.Today);
            var reservations = await _reservationRepository.ListAll(spec);
            var reservationViewModels = reservations.Select(rs => new ReservationViewModel(rs));

            return reservationViewModels;
        }
    }
}