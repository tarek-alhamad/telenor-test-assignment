﻿using Core.Entities.Reservations;
using Newtonsoft.Json;
using Web.Extensions;

namespace Web.Models.ReservationViewModels
{
    public class ReservationViewModel
    {
        public ReservationViewModel(Reservation reservation)
        {
            Email = reservation.ContactInfo.Email;
            EndTime = reservation.End.ToFormattedString();
            Id = reservation.Id;
            Name = reservation.ContactInfo.Name;
            PhoneNumber = reservation.ContactInfo.PhoneNumber;
            StartTime = reservation.Start.ToFormattedString();
        }

      
        [JsonProperty("email")]
        public string Email { get; private set; }

        [JsonProperty("endTime")]
        public string EndTime { get; private set; }

        [JsonProperty("id")]
        public int Id { get; private set; }
        
        [JsonProperty("name")]
        public string Name { get; private set; }

        [JsonProperty("phoneNumber")]
        public string PhoneNumber { get; private set; }

        [JsonProperty("startTime")]
        public string StartTime { get; private set; }
    }
}