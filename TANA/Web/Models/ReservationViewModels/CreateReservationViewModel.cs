﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Web.Models.ReservationViewModels
{
    public class CreateReservationViewModel
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("phoneNumber")]
        public string PhoneNumber { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("startTime")]
        public string StartTime { get; set; }

        [JsonProperty("endTime")]
        public string EndTime { get; set; }
        
    }
}