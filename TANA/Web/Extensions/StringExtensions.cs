﻿using System.Collections.Generic;
using System.Net.Mail;
using System.Text;

namespace Web.Extensions
{
    public static class StringExtensions
    {
        public static bool IsNullOrEmpty(this string value)
        {
            return string.IsNullOrEmpty(value);
        }

        public static bool IsNotValidEmail(this string value)
        {
            try
            {
                var address = new MailAddress(value);
                return address.Address != value;
            }
            catch
            {
                return true;
            }
        }

        public static string ConvertToString<T>(this IEnumerable<T> list, string separator = ", ")
        {
            if (list == null) return string.Empty;
            var convertedItems = new StringBuilder();
            var currentSeparator = string.Empty;
            foreach (var item in list)
            {
                var itemAsString = item.ToString();
                if (itemAsString.IsNullOrEmpty()) continue;
                convertedItems.Append($"{currentSeparator}{itemAsString}");
                currentSeparator = separator;
            }

            return convertedItems.ToString();
        }

    }
}