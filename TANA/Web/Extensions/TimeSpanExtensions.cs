﻿using System;
using System.Text.RegularExpressions;

namespace Web.Extensions
{
    public static class TimeSpanExtensions
    {
        private const string ValidTimeRegExp = @"^([0-9]|0[0-9]|1[0-9]|2[0-3]):([0-9]|[0-5][0-9])$";

        public static bool IsNotValidTimeFormat(this string input)
        {
            if (input.IsNullOrEmpty())
            {
                return true;
            }

            if (!Regex.IsMatch(input, ValidTimeRegExp))
            {
                return true;
            }

            return !TimeSpan.TryParse(input, out var outputTimeSpan);
        }

        public static bool IsValidTimeFormat(this string input)
        {
            if (input.IsNullOrEmpty())
            {
                return false;
            }
            if (!Regex.IsMatch(input, ValidTimeRegExp))
            {
                return false;
            }

            return TimeSpan.TryParse(input, out var outputTimeSpan);
        }

        public static TimeSpan ToTimeSpan(this string input)
        {
            TimeSpan.TryParse(input, out var outputTimeSpan);

            return outputTimeSpan;
        }

        public static string ToFormattedString(this TimeSpan timeSpan)
        {
            return timeSpan.Ticks >= 0 ? $"{new DateTime(timeSpan.Ticks):HH:mm}" : "00:00}";
        }

        public static TimeSpan GetTimeSpanDifference(this TimeSpan end, TimeSpan start)
        {
            return new TimeSpan(end.Ticks - start.Ticks);
        }
    }
}
