﻿using Microsoft.AspNetCore.Http;
using System;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    public interface IExceptionHandler
    {
        bool CanHandleException(Exception ex);
        Task FormatResponse(HttpResponse response);
    }
}
