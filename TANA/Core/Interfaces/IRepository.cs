﻿using Core.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    public interface IRepository<T> where T : BaseEntity
    {
        Task<T> AddAsync(T entity);
        Task AddList(IEnumerable<T> entities);
        Task Delete(T entity);
        Task<T> GetByIdAsync(int id);
        IEnumerable<T> ListAll();
        Task<List<T>> ListAll(ISpecification<T> spec);
    }
}