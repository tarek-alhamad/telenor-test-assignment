﻿namespace Core.Constants
{
    public static class ErrorMessages
    {
        public const string ArgumentException = "Operation is not accepted.";
        public const string ContactNameRequired = "Contact name is required";
        public const string ContactPhoneNumberRequired = "Contact phone number is required";
        public const string DbUpdateException = "Cannot add or update model. Correct the following error";
        public const string EndTimeBeforeStartTimeText = "Reservation End time cannot be before or equal StartTime";
        public const string Exception = "Operation Failed.";
        public const string MissingEmailAddress = "Please provide valid Contact email address";
        public const string ProvideValidTimeText = "Please provide valid time format for reservation";
    }
}
