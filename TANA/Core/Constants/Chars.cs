﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Constants
{
    public static class  Chars
    {
        public const string CurlyBracketsAsString = "{";
        public const string DashAsString = "-";
        public const string DotAsString = ".";
        public const string EndCurlyBracketsAsString = "}";
        public const string EndSquareBracketAsString = "]";
        public const string SquareBracketAsString = "[";
    }
}
