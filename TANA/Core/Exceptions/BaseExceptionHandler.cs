﻿using Core.Interfaces;
using Microsoft.AspNetCore.Http;
using System;
using System.Threading.Tasks;

namespace Core.Exceptions
{
    public abstract class BaseExceptionHandler : IExceptionHandler
    {
        public abstract bool CanHandleException(Exception ex);
        protected Exception Exception { get; set; }
        public abstract Task FormatResponse(HttpResponse response);
    }
}