﻿using System;
using System.Collections.Generic;
using System.Text;
using Core.Entities.Reservations;

namespace Core.Specifications.ReservationSpecifications
{
    public sealed class ReservationSpecification : BaseSpecification<Reservation>
    {
        /// <summary>
        /// Get daily reservation.
        /// </summary>
        /// <param name="date"></param>
        public ReservationSpecification(DateTime date) : base(re => re.Date.Date == date)
        {
        }
    }
}