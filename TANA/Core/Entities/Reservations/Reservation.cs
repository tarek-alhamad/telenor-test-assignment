﻿using System;
namespace Core.Entities.Reservations
{
    public class Reservation : BaseEntity
    {
        private Reservation()
        {
            // Required by EF
        }
        public Reservation(ContactInfo contactInfo, TimeSpan start, TimeSpan end)
        {
            ContactInfo = contactInfo;
            Start = start;
            End = end;
            Date = DateTime.Now.Date; //Requirement all reservation in the same day of creation
        }

        public ContactInfo ContactInfo { get; private set; }
        public DateTime Date { get; private set; }
        public TimeSpan End { get; private set; }
        public TimeSpan Start { get; private set; }
    }
}