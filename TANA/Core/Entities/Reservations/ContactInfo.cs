﻿namespace Core.Entities.Reservations
{
    public class ContactInfo
    {
        private ContactInfo()
        {
            //required by EF
        }
        public ContactInfo(string email, string name, string phoneNumber)
        {
            Email = email;
            Name = name;
            PhoneNumber = phoneNumber;
        }
        public string Email { get; private set; }
        public string Name { get; private set; }
        public string PhoneNumber { get; private set; }
    }
}